package cl.itf.j1c.server;

import cl.itf.j1c.server.form.J1CStartPane;
import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.slf4j.LoggerFactory;

/*
 * The server that can be run both as a console application or a GUI
 */
public class Server implements IClientlistener
{
    protected org.slf4j.Logger logger = LoggerFactory.getLogger(Server.class );

    // an ArrayList to keep the list of the Client
    private CopyOnWriteArrayList<ClientThread> managers;
    // if I am in a GUI
    private J1CStartPane sg;
    // to display time
    private SimpleDateFormat sdf;
    // the port number to listen for connection
    private int port;
    // the boolean that will be turned of to stop the server
    private boolean keepGoing = true;
    //
    private final String newLine = "\n";
    private ServerSocket serverSocket;
    private ExecutorService service;

    /*
     *  server constructor that receive the port to listen to for connection as parameter
     *  in console
     */
    public Server( int port )
    {
        this( port, null );
    }

    public Server( int port, J1CStartPane sg )
    {
        // GUI or not
        this.sg = sg;
        // the port
        this.port = port;
        // to display hh:mm:ss
        sdf = new SimpleDateFormat( "HH:mm:ss" );
        // ArrayList for the Client list
        managers = new CopyOnWriteArrayList<ClientThread>();
        //
        service = Executors.newFixedThreadPool( 50 );
    }

    /**
     *      */
    public void start()
    {
        /* create socket server and wait for connection requests */
        try
        {
            //open the socket server
            serverSocket = new ServerSocket( port );
        }
        catch ( IOException ioe )
        {
            display( "IOException setting up ServerSocket: " + ioe );
            return;
        }

        // infinite loop to wait for connections
        while ( keepGoing )
        {
            try
            {
                this.agentCleanup();

                // format message saying we are waiting
                display( "Server waiting for Clients on port " + port + "." );

                Socket socket = serverSocket.accept();             // accept connection

                ClientThread t = new ClientThread( socket ); // make a thread of it
                managers.add( t );                                 // save it in the ArrayList
                t.addClientListener( this );
                service.submit( t );
            }
            catch ( Throwable e )
            {
                display( "Socket closed " + e );
            }
        }

        for ( ClientThread tm : managers )
        {
            if ( !tm.isClosed() )
            {
                //issue shutdown command to the agent
                tm.close();
            }
        }

        this.agentCleanup();
    }

    /*
     * For the GUI to stop the server
     */
    public void stop()
    {
        keepGoing = false;
        
        try
        {
            if (serverSocket != null) {
                serverSocket.close();
            }
            if (service != null) {
                service.shutdown();
            }
        }
        catch ( IOException e )
        {
            // nothing I can really do
        }
        //pause briefly to let shutdown processing take action
        try
        {
            Thread.sleep( 250 );
        }
        catch ( InterruptedException e )
        {
            //do nothing
        }
    }

    /*
     * Display an event (not a message) to the console or the GUI
     */
    @Override
    public void display( String msg )
    {
        String time = sdf.format( new Date() ) + " " + msg;
        if ( sg == null )
        {
            logger.debug( time );
        }
        else
        {
            sg.appendEvent( time + newLine );
        }
    }

    /**
     * Removes old agents from the list so they may be garbage collected.
     */
    private void agentCleanup()
    {
        for ( ClientThread tm : managers )
        {
            if ( tm.isClosed() )
            {
                //remove this agent from the list
                managers.remove( tm );
            }//end if
        }//end for
    }
}
