package cl.itf.j1c.server;

import java.io.File;
import org.jawin.DispatchPtr;
import org.jawin.win32.Ole32;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jawin.COMException;

public class Procedure {

    private static final Logger logger = Logger.getLogger(Procedure.class.getName());

    /**
     * @param path
     * @param user
     * @param pass
     * <p>
     * @return
     * <p>
     * @throws java.io.IOException
     */
    public String AddComp(String path, String user, String pass) throws IOException {
        String res = "";
        FileHandler fh = null;
        DispatchPtr app = null;

        try {
            final String curDir = System.getProperty("user.dir")
                                  + File.separator + "log";
            final File file = new File(curDir);
            if (!file.exists()) {
                file.mkdir();
            }
            fh = new FileHandler(file.getAbsolutePath()
                                 + File.separator + "addbase.log");
            logger.addHandler(fh);
            logger.setLevel(Level.ALL);

            logger.info("Start Ole");
            Ole32.CoInitialize();
            logger.info("Start V8");

            try {
                app = new DispatchPtr("V83.Application");
            } catch (Throwable ex) {
                logger.warning(ex.getMessage());
                app = null;
            }

            if (app == null) {
                try {
                    app = new DispatchPtr("V82.Application");
                } catch (Throwable ex) {
                    logger.warning(ex.getMessage());
                    app = null;
                }
            }

            if (app == null) {
                try {
                    app = new DispatchPtr("V81.Application");
                } catch (Throwable ex) {
                    logger.warning(ex.getMessage());
                    app = null;
                }
            }

            if (app != null) {
                logger.info("Start connect");
                app.invoke("Connect", path + ";Usr=\"" + user + "\";Pwd=\"" + pass + "\"");
                logger.info("End connect");
                final DispatchPtr zapros = (DispatchPtr) app.invoke("NewObject", "Запрос");

                zapros.put("Text", "ВЫБРАТЬ Организации.Наименование КАК Org ИЗ "
                                 + " Справочник.Организации КАК Организации ГДЕ НЕ Организации.ПометкаУдаления ");

                final DispatchPtr result = (DispatchPtr) zapros.invoke("Execute");
                final DispatchPtr select = (DispatchPtr) result.invoke("Choose");

                while ((boolean) select.invoke("Next")) {
                    String org = (String) select.get("Org");
                    res = res + org + ";";
                    logger.info(org);
                }

                logger.info("Kill task 1c");
                try {
                    Runtime.getRuntime().exec("taskkill /F /IM 1cv8.exe");
                } catch (IOException e) {
                    logger.throwing(this.getClass().getName(), "taskkill exception ", e);
                }
            }

            logger.info("CoUninitialize Ole32");
            Ole32.CoUninitialize();
        } catch (Throwable e) {
            logger.throwing(this.getClass().getName(), "Add company error ", e);
            if (fh != null) {
                fh.close();
            }
            throw new IOException(e);
        } finally {
            try {
                if (app != null) {
                    app.close();
                    app = null;
                }
            } catch (COMException ex) {
                logger.throwing(this.getClass().getName(), "Application close error ", ex);
            }
        }

        return res;
    }

    /**
     *
     * @param path
     * @param user
     * @param pass
     * @param savepath
     * @param company
     * @param label
     * @param reports
     * <p>
     * @return
     * <p>
     * @throws java.io.IOException
     */
    public String RepGen(String path, String user, String pass, String savepath,
                         String company, String label, String reports) throws IOException {
        String res = "";
        FileHandler fh = null;

        DispatchPtr app = null;
        try {
            final String curDir = System.getProperty("user.dir")
                                  + File.separator + "log";
            final File file = new File(curDir);
            if (!file.exists()) {
                file.mkdir();
            }
            fh = new FileHandler(file.getAbsolutePath()
                                 + File.separator + "repgen.log");
            logger.addHandler(fh);
            logger.setLevel(Level.ALL);

            logger.info("Start V8.Application");

            try {
                app = new DispatchPtr("V83.Application");
            } catch (Throwable ex) {
                logger.warning(ex.getMessage());
                app = null;
            }

            if (app == null) {
                try {
                    app = new DispatchPtr("V82.Application");
                } catch (Throwable ex) {
                    logger.warning(ex.getMessage());
                    app = null;
                }
            }

            if (app == null) {
                try {
                    app = new DispatchPtr("V81.Application");
                } catch (Throwable ex) {
                    logger.warning(ex.getMessage());
                    app = null;
                }
            }

            if (app != null) {
                logger.info("Connect");
                app.invoke("Connect", path + ";Usr=\"" + user + "\";Pwd=\"" + pass + "\"");
                logger.info("Connect successful");

                DispatchPtr comp = (DispatchPtr) app.get("Справочники");
                comp = (DispatchPtr) comp.get("Организации");

                DispatchPtr org = (DispatchPtr) comp.invoke("НайтиПоНаименованию", company, "Истина");

                DispatchPtr ft = (DispatchPtr) app.get("SpreadsheetDocumentFileType");
                ft = (DispatchPtr) ft.get("XLS");

                final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                final StringBuilder strBldr = new StringBuilder();

                File dir = new File(savepath);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                logger.log(Level.INFO, "test - {0}", savepath);

                String[] srcdata = reports.split("#");

                for (String getsrcdata : srcdata) {
                    String[] data = getsrcdata.split(";");

                    if (data.length > 4) {
                        String dt = data[1];
                        String accN = data[2];
                        String types = data[4];

                        DispatchPtr acc = (DispatchPtr) app.get("ChartsOfAccounts");
                        acc = (DispatchPtr) acc.get("Хозрасчетный");
                        acc = (DispatchPtr) acc.invoke("FindByCode", accN);

                        String db;
                        String de;

                        if (dt.contains("-")) {
                            String[] datess = dt.split("/");

                            String[] tmpD1 = datess[0].split("-");
                            String[] tmpD2 = datess[1].split("-");
                            db = tmpD1[2].trim() + "-" + tmpD1[1].trim() + "-" + tmpD1[0].trim();
                            de = tmpD2[2].trim() + "-" + tmpD2[1].trim() + "-" + tmpD2[0].trim();
                        } else {
                            String[] datess = dt.split("/");

                            String[] tmpD1 = datess[0].split(".");
                            String[] tmpD2 = datess[1].split(".");
                            db = tmpD1[2].trim() + "-" + tmpD1[1].trim() + "-" + tmpD1[0].trim();
                            de = tmpD2[2].trim() + "-" + tmpD2[1].trim() + "-" + tmpD2[0].trim();
                        }

                        logger.log(Level.INFO, "{0} / {1}", new Object[]{db, de});

                        //ОборотноСальдоваяВедомость
                        if (types.equals("0")) {
                            try {
                                DispatchPtr zapros = (DispatchPtr) app.invoke("NewObject", "ТабличныйДокумент");
                                DispatchPtr ОтчетОбъект = (DispatchPtr) app.get("Reports");

                                ОтчетОбъект = (DispatchPtr) ОтчетОбъект.get("ОборотноСальдоваяВедомость");
                                ОтчетОбъект = (DispatchPtr) ОтчетОбъект.invoke("Создать");

                                DispatchPtr ФормаОтчета = (DispatchPtr) ОтчетОбъект.invoke("GetForm");
                                ФормаОтчета = (DispatchPtr) ФормаОтчета.invoke("Open");
                                ОтчетОбъект.put("НачалоПериода", sdf.parseObject(db));
                                ОтчетОбъект.put("КонецПериода", sdf.parseObject(de));
                                ОтчетОбъект.put("ПоСубсчетам", "Истина");
                                ОтчетОбъект.put("Организация", org);

                                ОтчетОбъект.invoke("СформироватьОтчет", zapros);

                                strBldr.append(savepath).append(File.separator)
                                       .append(label).append(db).append("_")
                                       .append(de)
                                       .append("_ОСВ.xls");
                                zapros = (DispatchPtr) zapros.invoke("Write", strBldr.toString(), ft);
                                strBldr.setLength(0);
                                logger.info("Оборотно Сальдовая Ведомость");
                            } catch (Throwable ex) {
                                logger.warning(ex.getMessage());
                            }
                        }

                        //ОборотноСальдоваяВедомостьПоСчету
                        if (types.equals("1")) {
                            try {
                                DispatchPtr zapros = (DispatchPtr) app.invoke("NewObject", "ТабличныйДокумент");
                                DispatchPtr ОтчетОбъект = (DispatchPtr) app.get("Reports");

                                ОтчетОбъект = (DispatchPtr) ОтчетОбъект.get("ОборотноСальдоваяВедомостьПоСчету");
                                ОтчетОбъект = (DispatchPtr) ОтчетОбъект.invoke("Создать");

                                DispatchPtr ФормаОтчета = (DispatchPtr) ОтчетОбъект.invoke("GetForm");
                                ФормаОтчета = (DispatchPtr) ФормаОтчета.invoke("Open");

                                ОтчетОбъект.put("НачалоПериода", sdf.parseObject(db));
                                ОтчетОбъект.put("КонецПериода", sdf.parseObject(de));
                                ОтчетОбъект.put("ПоСубсчетам", "Истина");
                                ОтчетОбъект.put("Организация", org);
                                ОтчетОбъект.put("Счет", acc);

                                ОтчетОбъект.invoke("СформироватьОтчет", zapros);

                                strBldr.append(savepath).append(File.separator)
                                       .append(label).append(db).append("_")
                                       .append(de);
                                if (accN != null && !accN.isEmpty()) {
                                    strBldr.append("_ОСВПоСчету_").append(accN);
                                } else {
                                    strBldr.append("_ОСВПоСчету");
                                }
                                strBldr.append(".xls");

                                zapros = (DispatchPtr) zapros.invoke("Write", strBldr.toString(), ft);
                                strBldr.setLength(0);
                                logger.info("Оборотно Сальдовая Ведомость По Счету");
                            } catch (Throwable ex) {
                                logger.warning(ex.getMessage());
                            }
                        }

                        //КарточкаСчета
                        if ("2".equals(types)) {
                            try {
                                DispatchPtr zapros = (DispatchPtr) app.invoke("NewObject", "ТабличныйДокумент");
                                DispatchPtr ОтчетОбъект = (DispatchPtr) app.get("Reports");

                                ОтчетОбъект = (DispatchPtr) ОтчетОбъект.get("КарточкаСчета");
                                ОтчетОбъект = (DispatchPtr) ОтчетОбъект.invoke("Создать");

                                DispatchPtr ФормаОтчета = (DispatchPtr) ОтчетОбъект.invoke("GetForm");
                                ФормаОтчета = (DispatchPtr) ФормаОтчета.invoke("Open");

                                ОтчетОбъект.put("НачалоПериода", sdf.parseObject(db));
                                ОтчетОбъект.put("КонецПериода", sdf.parseObject(de));
                                ОтчетОбъект.put("Организация", org);
                                ОтчетОбъект.put("Периодичность", 0);
                                ОтчетОбъект.put("Счет", acc);

                                ОтчетОбъект.invoke("СформироватьОтчет", zapros);

                                strBldr.append(savepath).append(File.separator)
                                       .append(label).append(db).append("_")
                                       .append(de);
                                if (accN != null && !accN.isEmpty()) {
                                    strBldr.append("_КарточкаСчета_").append(accN);
                                } else {
                                    strBldr.append("_КарточкаСчета");
                                }
                                strBldr.append(".xls");
                                zapros = (DispatchPtr) zapros.invoke("Write", strBldr.toString(), ft);
                                strBldr.setLength(0);
                                logger.info("КарточкаСчета");
                            } catch (Throwable ex) {
                                logger.warning(ex.getMessage());
                            }
                        }

                        //АнализСчета
                        if ("3".equals(types)) {
                            try {
                                DispatchPtr zapros = (DispatchPtr) app.invoke("NewObject", "ТабличныйДокумент");
                                DispatchPtr ОтчетОбъект = (DispatchPtr) app.get("Reports");

                                ОтчетОбъект = (DispatchPtr) ОтчетОбъект.get("АнализСчета");
                                ОтчетОбъект = (DispatchPtr) ОтчетОбъект.invoke("Создать");

                                DispatchPtr ФормаОтчета = (DispatchPtr) ОтчетОбъект.invoke("GetForm");
                                ФормаОтчета = (DispatchPtr) ФормаОтчета.invoke("Open");

                                ОтчетОбъект.put("НачалоПериода", sdf.parseObject(db));
                                ОтчетОбъект.put("КонецПериода", sdf.parseObject(de));
                                ОтчетОбъект.put("ПоСубсчетам", "Истина");
                                ОтчетОбъект.put("ПоСубсчетамКорСчетов", "Истина");
                                ОтчетОбъект.put("Организация", org);
                                ОтчетОбъект.put("Периодичность", 0);
                                ОтчетОбъект.put("Счет", acc);

                                ОтчетОбъект.invoke("СформироватьОтчет", zapros);

                                strBldr.append(savepath).append(File.separator)
                                       .append(label).append(db).append("_")
                                       .append(de);
                                if (accN != null && !accN.isEmpty()) {
                                    strBldr.append("_АнализСчета_").append(accN);
                                } else {
                                    strBldr.append("_АнализСчета");
                                }
                                strBldr.append(".xls");
                                zapros = (DispatchPtr) zapros.invoke("Write", strBldr.toString(), ft);
                                strBldr.setLength(0);
                                logger.info("АнализСчета");
                            } catch (Throwable ex) {
                                logger.warning(ex.getMessage());
                            }
                        }

                        //АнализСчетаПоСубконто
                        if (types.equals("4")) {
                            try {
                                DispatchPtr zapros = (DispatchPtr) app.invoke("NewObject", "ТабличныйДокумент");
                                DispatchPtr ОтчетОбъект = (DispatchPtr) app.get("Reports");

                                ОтчетОбъект = (DispatchPtr) ОтчетОбъект.get("АнализСчета");
                                ОтчетОбъект = (DispatchPtr) ОтчетОбъект.invoke("Создать");

                                DispatchPtr ФормаОтчета = (DispatchPtr) ОтчетОбъект.invoke("GetForm");
                                ФормаОтчета = (DispatchPtr) ФормаОтчета.invoke("Open");

                                ОтчетОбъект.put("НачалоПериода", sdf.parseObject(db));
                                ОтчетОбъект.put("КонецПериода", sdf.parseObject(de));
                                ОтчетОбъект.put("ПоСубсчетам", "Истина");
                                ОтчетОбъект.put("ПоСубсчетамКорСчетов", "Истина");
                                ОтчетОбъект.put("Организация", org);
                                ОтчетОбъект.put("Периодичность", 0);
                                ОтчетОбъект.put("Счет", acc);

                                ОтчетОбъект.invoke("СформироватьОтчет", zapros);

                                strBldr.append(savepath).append(File.separator)
                                       .append(label).append(db).append("_")
                                       .append(de);
                                if (accN != null && !accN.isEmpty()) {
                                    strBldr.append("_АнализСчетаПоСубконто_").append(accN);
                                } else {
                                    strBldr.append("_АнализСчетаПоСубконто");
                                }
                                strBldr.append(".xls");
                                zapros = (DispatchPtr) zapros.invoke("Write", strBldr.toString(), ft);
                                strBldr.setLength(0);
                                logger.info("АнализСчетаПоСубконто");
                            } catch (Throwable ex) {
                                logger.warning(ex.getMessage());
                            }
                        }

                        //АнализСубконто
                        if ("5".equals(types)) {
                            try {
                                DispatchPtr zapros = (DispatchPtr) app.invoke("NewObject", "ТабличныйДокумент");
                                DispatchPtr ОтчетОбъект = (DispatchPtr) app.get("Reports");

                                ОтчетОбъект = (DispatchPtr) ОтчетОбъект.get("АнализСубконто");
                                ОтчетОбъект = (DispatchPtr) ОтчетОбъект.invoke("Создать");

                                DispatchPtr ФормаОтчета = (DispatchPtr) ОтчетОбъект.invoke("GetForm");
                                ФормаОтчета = (DispatchPtr) ФормаОтчета.invoke("Open");

                                ОтчетОбъект.put("НачалоПериода", sdf.parseObject(db));
                                ОтчетОбъект.put("КонецПериода", sdf.parseObject(de));
                                ОтчетОбъект.put("ПоСубсчетам", "Истина");
                                ОтчетОбъект.put("Организация", org);
                                ОтчетОбъект.put("Периодичность", 0);

                                ОтчетОбъект.invoke("СформироватьОтчет", zapros);

                                strBldr.append(savepath).append(File.separator)
                                       .append(label).append(db).append("_")
                                       .append(de)
                                       .append("_АнализСубконто.xls");
                                zapros = (DispatchPtr) zapros.invoke("Write", strBldr.toString(), ft);
                                strBldr.setLength(0);
                                logger.info("АнализСубконто");
                            } catch (Throwable ex) {
                                logger.warning(ex.getMessage());
                            }
                        }

                        //ОтчетПоПроводкам
                        if ("6".equals(types)) {
                            try {
                                DispatchPtr zapros = (DispatchPtr) app.invoke("NewObject", "ТабличныйДокумент");
                                DispatchPtr ОтчетОбъект = (DispatchPtr) app.get("Reports");

                                ОтчетОбъект = (DispatchPtr) ОтчетОбъект.get("ОтчетПоПроводкам");
                                ОтчетОбъект = (DispatchPtr) ОтчетОбъект.invoke("Создать");

                                DispatchPtr ФормаОтчета = (DispatchPtr) ОтчетОбъект.invoke("GetForm");
                                ФормаОтчета = (DispatchPtr) ФормаОтчета.invoke("Open");

                                ОтчетОбъект.put("НачалоПериода", sdf.parseObject(db));
                                ОтчетОбъект.put("КонецПериода", sdf.parseObject(de));
                                ОтчетОбъект.put("Организация", org);
                                ОтчетОбъект.invoke("СформироватьОтчет", zapros);

                                strBldr.append(savepath).append(File.separator)
                                       .append(label).append(db).append("_")
                                       .append(de)
                                       .append("_ОтчетПоПроводкам.xls");
                                zapros = (DispatchPtr) zapros.invoke("Write", strBldr.toString(), ft);
                                strBldr.setLength(0);
                                logger.info("ОтчетПоПроводкам");
                            } catch (Throwable ex) {
                                logger.warning(ex.getMessage());
                            }
                        }

                        acc.close();
                    }
                }
            }
        } catch (Throwable e) {
            String result = new String(e.getMessage().getBytes("windows-1252"), Charset.forName("windows-1251"));
            logger.throwing(this.getClass().getName(), "Rep generating error ", e);
            if (fh != null) {
                fh.close();
            }
            throw new IOException(result);
        } finally {
            try {
                if (app != null) {
                    app.close();
                    app = null;
                }
            } catch (COMException ex) {
                logger.throwing(this.getClass().getName(), "Application close error ", ex);
            }
        }

        return res;
    }
}
