package cl.itf.j1c.server;

import cl.itf.j1c.chatmessage.ChatMessage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.concurrent.CopyOnWriteArrayList;
import org.slf4j.LoggerFactory;

/**
 * @author sniffl
 */
public class ClientThread implements Runnable {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(ClientThread.class);

    // the socket where to listen/talk
    private final Socket socket;
    private ObjectInputStream sInput;
    private ObjectOutputStream sOutput;
    // the Username of the Client
    private String username;
    // the only type of message a will receive
    private ChatMessage cm;
    //
    private final CopyOnWriteArrayList<IClientlistener> listeners;

    // Constructore
    public ClientThread(Socket socket) {
        this.socket = socket;
        this.listeners = new CopyOnWriteArrayList<>();
    }

    public void login() {
        /* Creating both Data Stream */
        logger.debug("Thread trying to create Object Input/Output Streams");
        try {
            // create output first
            sOutput = new ObjectOutputStream(socket.getOutputStream());
            sInput = new ObjectInputStream(socket.getInputStream());
            // read the username
            username = (String) sInput.readObject();
            fireClientListener(username + " just connected.");
            sOutput.writeObject(new ChatMessage(ChatMessage.ACCEPTED));
        } catch (IOException e) {
            logger.debug("Exception creating new Input/output Streams: " + e);
        } catch (ClassNotFoundException e) {
            logger.debug(e.getMessage());
            try {
                sOutput.writeObject(new ChatMessage(ChatMessage.ERROR));
            } catch (IOException ex) {
                logger.debug("Exception creating new Input/output Streams: " + ex);
            }
        }
    }

    public void addClientListener(IClientlistener listener) {
        listeners.add(listener);
    }

    public void removeClientListener(IClientlistener listener) {
        listeners.remove(listener);
    }

    public void fireClientListener(String message) {
        for (IClientlistener listener : listeners) {
            listener.display(message);
        }
    }

    // what will run forever
    @Override
    public void run() {
        login();

        // to loop until LOGOUT
        boolean keepGoing = true;
        while (keepGoing) {
            // read a String (which is an object)
            try {
                cm = (ChatMessage) sInput.readObject();
            } catch (IOException e) {
                logger.debug(username + " Exception reading Streams: " + e);
                break;
            } catch (ClassNotFoundException e2) {
                logger.debug(e2.getMessage());
                break;
            }//end try

            // the messaage part of the ChatMessage
            String message = cm.getMessage();

            // Switch on the type of message receive
            switch (cm.getType()) {
                case ChatMessage.MESSAGE:
                    broadcast(username + ": " + message);
                    keepGoing = false;
                    break;
                case ChatMessage.LOGOUT:
                    logger.debug(username + " disconnected with a LOGOUT message.");
                    keepGoing = false;
                    break;
            }//end switch
        }//end while

        close();
    }

    // try to close everything
    public void close() {
        // try to close the connection
        try {
            if (sOutput != null) {
                sOutput.close();
            }
            logger.debug("close output stream");
        } catch (IOException e) {
            logger.debug(e.getMessage());
        }

        try {
            if (sInput != null) {
                sInput.close();
            }
            logger.debug("close input stream");
        } catch (IOException e) {
            logger.debug(e.getMessage());
        }

        try {
            if (socket != null) {
                socket.close();
                fireClientListener(username + " disconnected.");
            }
            logger.debug("close socket");
        } catch (IOException e) {
            logger.debug("error when close: " + e.getMessage());
        }
    }

    /*
     *  to broadcast a message to all Clients
     */
    public void broadcast(String message) {
        logger.debug(message);
        ChatMessage result = null;

        String[] val = message.split(ChatMessage.delim);
        try {
            if (val.length == 0) {
                throw new IOException("Сообщение не соответствует формату");
            }

            final Procedure proc = new Procedure();
            
            if (val[0].contains(ChatMessage.REPGENIND)) {
                String path = "";
                String user = "";
                String pass = "";
                String savepath = "";
                String company = "";
                String label = "";
                String reports = "";

                for (int i = 1; i < val.length; i++) {
                    switch (i) {
                        case 1:
                            path = val[i];
                            break;
                        case 2:
                            user = val[i];
                            break;
                        case 3:
                            pass = val[i];
                            break;
                        case 4:
                            savepath = val[i];
                            break;
                        case 5:
                            company = val[i];
                            break;
                        case 6:
                            label = val[i];
                            break;
                        case 7:
                            reports = val[i];
                            break;
                    }
                }

                logger.debug("Go for report generate");
                logger.debug("1-" + path + " 2-" + user + " 3-" + pass + " 4-" + savepath
                            + " 5-" + company + " 6-" + label + " 7-" + reports);
                String CurRowEd = proc.RepGen(path, user == null ? "" : user, pass == null ? "" : pass, 
                                              savepath, company, label, reports);

                result = new ChatMessage(ChatMessage.RESULT, CurRowEd);
                logger.debug(CurRowEd);
            }//end if

            if (val[0].contains(ChatMessage.GETFIRMIND)) {
                String path = "";
                String user = "";
                String pass = "";

                for (int i = 1; i < val.length; i++) {
                    switch (i) {
                        case 1:
                            path = val[i];
                            break;
                        case 2:
                            user = val[i];
                            break;
                        case 3:
                            pass = val[i];
                            break;
                    }
                }

                String comp = proc.AddComp(path, user == null ? "" : user, pass == null ? "" : pass);
                result = new ChatMessage(ChatMessage.RESULT, comp);
                logger.debug(comp);
            }//end if
        } catch (Throwable e) {
            String errMes;
            try {
                errMes = new String(e.toString().getBytes(), "UTF-8");
                result = new ChatMessage(ChatMessage.ERROR, errMes);
                logger.debug(e.getMessage());
            } catch (UnsupportedEncodingException ex) {
                logger.debug(ex.getMessage());
            }
        }

        // try to write to the Client if it fails remove it from the list
        if (!writeMsg(result)) {
            logger.debug("Disconnected Client " + username + " removed from list.");
        }
    }

    /*
     * Write a String to the Client output stream
     */
    private boolean writeMsg(ChatMessage msg) {
        // if Client is still connected send the message to it
        if (!socket.isConnected()) {
            logger.debug("write mes error");
            close();
            return false;
        }

        // write the message to the stream
        try {
            sOutput.writeObject(msg);
            logger.debug("send mess to client");
        } // if an error occurs, do not abort just inform the user
        catch (IOException e) {
            logger.debug("Error sending message to " + username);
            logger.debug(e.toString());
            return false;
        }
        return true;
    }

    /**
     * Returns the state of the socket.
     * <p>
     * @return socket status
     */
    public boolean isClosed() {
        boolean isClosed = false;
        if (socket.isClosed()) {
            isClosed = true;
        }//end if

        return isClosed;
    }
}
