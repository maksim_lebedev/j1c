package cl.itf.j1c.server;

import cl.itf.j1c.server.form.J1CStartPane;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * @author sniffl
 */
public class StartServer
{
    public static void main( String args[] )
    {
        String laf = UIManager.getSystemLookAndFeelClassName();
        
        /* 
         * Set the Nimbus look and feel
         * <editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
         * If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try
        {
            UIManager.setLookAndFeel( laf );
        }
        catch ( Throwable ex )
        {
            JOptionPane.showMessageDialog( new javax.swing.JFrame(),
                                           ex.getMessage(),
                                           "WARNING", JOptionPane.WARNING_MESSAGE );
        }
        //</editor-fold>

        /* Create and display the form */
        SwingUtilities.invokeLater( new Runnable()
        {
            @Override
            public void run()
            {
                final J1CStartPane pane = new J1CStartPane();
                pane.show( null, "Log Window" );
            }
        } );
    }
}
