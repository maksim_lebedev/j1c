package cl.itf.j1c.server.form;

/**
 * @author lebedev-m
 */
public enum ITFEnumRetCode {
    RET_OK, // все хорошо
    RET_FAIL, // ошибка
    RET_CANCEL // отмена
}
