package cl.itf.j1c.server.form;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Window;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @author lebedev-m
 */
public class ITFPane extends JPanel {

    public static final int VM_NONE = 0, // не определено
                            VM_VIEW = 1, // отобразить
                            VM_INS = 2, // добавить
                            VM_EDIT = 3, // изменить
                            VM_DEL = 4, // удалить
                            VM_CHOICE = 5, // при выборе
                            VM_SHOW = 6; // вызов Show

    protected ITFEnumRetCode retCode = ITFEnumRetCode.RET_CANCEL;
    protected Window window = null;
    protected int m_vm = VM_NONE;

    /**
     * @param parentWindow
     * @param title
     * @param vm
     * <p>
     * @return
     */
    public ITFEnumRetCode doModal(Window parentWindow, String title, int vm) {
        JDialog dlg;
        this.m_vm = vm;
        if (parentWindow == null) {
            dlg = new JDialog();
            dlg.setTitle(title);
        } else if (parentWindow instanceof JFrame) {
            dlg = new JDialog((JFrame) parentWindow, title);
        } else {
            dlg = new JDialog((JDialog) parentWindow, title);
        }//end if-else

        dlg.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        dlg.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                close(ITFEnumRetCode.RET_CANCEL);
            }
        });

        Container cont = dlg.getContentPane();
        cont.setLayout(new BorderLayout());
        cont.add(this, BorderLayout.CENTER);
        this.window = dlg;
        this.init();
        initChildInvPanel();

        dlg.pack();

        dlg.setLocationRelativeTo(parentWindow);
        dlg.setModal(true);
        dlg.setVisible(true);
        return retCode;
    }

    /**
     * @param parentWindow
     * @param title
     */
    public void show(Window parentWindow, String title) {
        JFrame frm = new JFrame(title);
        this.m_vm = VM_SHOW;
        frm.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        frm.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                close(ITFEnumRetCode.RET_CANCEL);
            }
        });

        Container cont = frm.getContentPane();
        cont.setLayout(new BorderLayout());
        cont.add(this, BorderLayout.CENTER);
        this.window = frm;
        this.init();
        initChildInvPanel();

        frm.pack();

        ITFCompLocation.setOnCenter(frm, parentWindow);
        frm.setVisible(true);
    }

    /**
     *      */
    protected void initChildInvPanel() {
        ArrayList<ITFPane> array = new ArrayList<>();
        getChildInvPanel(array);
        for (ITFPane p : array) {
            p.init();
            p.setWindow(window);
        }//end for
    }

    /**
     * @param array
     */
    protected void getChildInvPanel(ArrayList<ITFPane> array) {
        Component children[] = getComponents();
        if (children == null) {
            return;
        }//end if

        for (Component child : children) {
            if (child instanceof ITFPane) {
                array.add((ITFPane) child);
                ((ITFPane) child).getChildInvPanel(array);
            } else if (child instanceof JComponent) {
                getChildInvPanel((JComponent) child, array);
            }//end if-else
        }//end for
    }

    /**
     * @param comp
     * @param array
     */
    protected void getChildInvPanel(JComponent comp, ArrayList<ITFPane> array) {
        if (comp == null) {
            return;
        }//end if

        Component children[] = comp.getComponents();
        if (children == null) {
            return;
        }//end if

        for (Component child : children) {
            if (child instanceof ITFPane) {
                array.add((ITFPane) child);
                ((ITFPane) child).getChildInvPanel(array);
            } else if (child instanceof JComponent) {
                getChildInvPanel((JComponent) child, array);
            }//end if-else
        }//end for
    }

    /**
     * @param wind
     */
    public void setWindow(Window wind) {
        window = wind;
    }

    /**
     * @return
     */
    public Window getWindow() {
        return window;
    }

    /**
     *      */
    protected void init() {

    }

    /**
     *      */
    public void onChoice() {

    }

    /**
     *      */
    public void onInsert() {

    }

    /**
     *      */
    public void onEdit() {

    }

    /**
     *      */
    public void onDelete() {

    }

    public void onProcess(ITFEnumRetCode retCode) {
        if (retCode == ITFEnumRetCode.RET_OK) {
            close(retCode);
        }//end if
    }

    /**
     *      */
    public void onCancel() {
        close(ITFEnumRetCode.RET_CANCEL);
    }

    /**
     *      */
    public void onClose() {
        close(ITFEnumRetCode.RET_CANCEL);
    }

    /**
     * @return
     */
    protected int getViewMode() {
        ITFPane topPanel = getTopPanel();
        if (topPanel != null) {
            return topPanel.m_vm;
        }
        return VM_SHOW;
    }

    /**
     * @return
     */
    protected ITFPane getTopPanel() {
        ITFPane panel = this;
        Container parent = getParent();

        while (parent != null && (parent instanceof Window) == false) {
            if (parent instanceof ITFPane) {
                panel = (ITFPane) parent;
                break;
            }
            parent = parent.getParent();
        }

        return panel;
    }

    /**
     * @param retCode
     */
    protected void close(ITFEnumRetCode retCode) {
        this.retCode = retCode;
        window.dispose();
    }
}
