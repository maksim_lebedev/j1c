package cl.itf.j1c.server.form;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;

/**
 * @author lebedev-m
 */
public class ITFCompLocation {

    /**
     * @param comp
     * @param parent 
     */
    public static void setOnCenter(Component comp, Component parent) {
        if (comp == null) {
            return;
        }//end if
        int left;
        int top;
        if (parent == null) {
            setOnScreenCenter(comp);
        } else {
            Point pParCenter = getCenter(parent);
            Dimension dSize = comp.getSize();
            left = pParCenter.x - dSize.width / 2;
            top = pParCenter.y - dSize.height / 2;
            if (left < 0) {
                left = 0;
            }//end if
            if (top < 0) {
                top = 0;
            }//end if
            comp.setLocation(left, top);
        }//end if-else
    }

    /**
     * @param comp 
     */
    public static void setOnScreenCenter(Component comp) {
        if (comp == null) {
            return;
        }//end if
        int left;
        int top;
        Point pScreenCenter = getScreenCenter();
        Dimension dSize = comp.getSize();
        left = pScreenCenter.x - dSize.width / 2;
        top = pScreenCenter.y - dSize.height / 2;
        if (left < 0) {
            left = 0;
        }//end if
        if (top < 0) {
            top = 0;
        }//end if
        comp.setLocation(left, top);
    }

    /**
     * @return  
     */
    public static Point getScreenCenter() {
        Dimension dScreen = Toolkit.getDefaultToolkit().getScreenSize();
        Point pCenter = new Point(dScreen.width / 2, dScreen.height / 2);
        return pCenter;
    }

    /**
     * @param comp
     * @return  
     */
    public static Point getCenter(Component comp) {
        Point pLoc = comp.getLocationOnScreen();
        int width = comp.getSize().width;
        int height = comp.getSize().height;
        Point pCenter = new Point(pLoc.x + width / 2, pLoc.y + height / 2);
        return pCenter;
    }
}
