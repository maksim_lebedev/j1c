package cl.itf.j1c.server;

/**
 * @author sniffl
 */
public interface IClientlistener
{
    public void display( String msg );
}
